package com.boatmodel;

import java.util.Date;



public class Boat implements Comparable<Boat> {
	private Long id;
	private String name;
	private String type;
	private Date date;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public Boat() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public Boat(String name, String type, Date date) {
		super();
		this.name = name;
		this.type = type;
		this.date = date;
	}
	@Override
	public String toString() {
		return "Boat [name=" + name + ", type=" + type + ", date=" + date + "]";
	}
	public int compareTo(Boat o) {
		return this.name.compareTo(o.getName());
	}

}