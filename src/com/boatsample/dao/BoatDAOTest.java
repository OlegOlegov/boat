package com.boatsample.dao;
import static org.junit.Assert.*;

import java.util.Date;
import java.util.List;

import org.junit.Test;

import com.boat.dao.BoatDAO;
import com.boatmodel.Boat;



public class BoatDAOTest {

	@Test
	public void testDao() {
		BoatDAO.getInstance().deleteAll();
		assertEquals(0, BoatDAO.getInstance().selectAll().size());

		Boat b = new Boat("abc", "bcd", new Date());
		BoatDAO.getInstance().insert(b);

		List<Boat> books = BoatDAO.getInstance().selectAll();
		assertEquals(1, books.size());
		assertEquals(b.getType(), books.get(0).getType());
		assertEquals(b.getName(), books.get(0).getName());
		assertEquals(b.getDate(), books.get(0).getDate());
		BoatDAO.getInstance().delete(books.get(0));
		assertEquals(0, BoatDAO.getInstance().selectAll().size());

		BoatDAO.getInstance().insert(b);
		BoatDAO.getInstance().deleteAll();
		assertEquals(0, BoatDAO.getInstance().selectAll().size());
	}
}
