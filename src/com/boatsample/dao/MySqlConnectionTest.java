package com.boatsample.dao;
import static org.junit.Assert.*;

import java.sql.SQLException;

import org.junit.Test;

import com.boat.dao.MySqlConnection;

public class MySqlConnectionTest {

	@Test
	public void testGetConnection() throws SQLException {
		assertFalse(MySqlConnection.getConnection().isClosed());
	}

}

