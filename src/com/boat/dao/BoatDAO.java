package com.boat.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.boatmodel.Boat;



public class BoatDAO {
	private static String SQL_INSERT = "INSERT INTO boats (name, type, date) VALUE (?, ?, ?); ";
	private static String SQL_UPDATE = "UPDATE boats set name=?, type=?, date=? where id=?";
	private static String SQL_SELECT = "SELECT * FROM boats;";
	private static String SQL_DELETE = "delete from boats;";
	private static String SQL_SELECT_BY_ID = "SELECT * FROM boats where id = ?;";
	private static String SQL_DELETE_BY_ID = "delete from boats where id = ?;";
	private static final String FIELD_ID = "id";
	private static final String FIELD_NAME = "name";
	private static final String FIELD_TYPE = "type";
	private static final String FIELD_DATE = "date";
	private static BoatDAO BOAT_DAO = null;

	private BoatDAO() {

	}

	public static BoatDAO getInstance() {
		if (BOAT_DAO == null) {
			BOAT_DAO = new BoatDAO();
		}
		return BOAT_DAO;
	}

	public boolean delete(Boat item) {
		return delete(item.getId());
	}

	public boolean delete(long id) {
		Connection con = null;
		try {
			con = MySqlConnection.getConnection();
			PreparedStatement st = con.prepareStatement(SQL_DELETE_BY_ID);
			st.setLong(1, id);
			st.executeUpdate();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			close(con);
		}
	}

	public boolean deleteAll() {
		Connection con = null;
		try {
			con = MySqlConnection.getConnection();
			PreparedStatement st = con.prepareStatement(SQL_DELETE);
			int n = st.executeUpdate();
			if (n == 0) {
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			close(con);
		}
	}

	public List<Boat> selectAll() {
		Connection con = null;
		List<Boat> list = new ArrayList<Boat>();
		try {
			con = MySqlConnection.getConnection();
			Statement st = con.createStatement();
			ResultSet result = st.executeQuery(SQL_SELECT);
			while (result.next()) {
				list.add(extractBoat(result));
			}
		} catch (Exception e) {
			e.printStackTrace();
			return list;
		} finally {
			close(con);
		}
		return list;
	}

	public Boat select(int id) {
		Connection con = null;
		Boat item = null;
		try {
			con = MySqlConnection.getConnection();
			PreparedStatement st = con.prepareStatement(SQL_SELECT_BY_ID);
			st.setInt(1, id);
			ResultSet result = st.executeQuery();
			if (result.next()) {
				item = extractBoat(result);
			}
			return item;
		} catch (Exception e) {
			e.printStackTrace();
			return item;
		} finally {
			close(con);
		}
	}

	/**
	 * Method insert object into database
	 * 
	 * @param item
	 *            - the book will be inserted
	 * @return true if successfully inserted
	 */
	public boolean insert(Boat item) {
		return insert(item.getName(), item.getType(), item.getDate());
	}

	public boolean insert(String name, String type, Date date) {
		Connection con = null;
		try {
			con = MySqlConnection.getConnection();
			PreparedStatement st = con.prepareStatement(SQL_INSERT);
			st.setString(1, name);
			st.setString(2, type);
			st.setLong(3, date.getTime());
			int n = st.executeUpdate();
			if (n == 0) {
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			close(con);
		}
	}

	public void update(Boat item) {
		Connection con = null;
		try {
			con = MySqlConnection.getConnection();
			PreparedStatement preparedStatement = con
					.prepareStatement(SQL_UPDATE);
			preparedStatement.setString(1, item.getName());
			preparedStatement.setString(2, item.getType());
			preparedStatement.setLong(3, item.getDate().getTime());
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(con);
		}
	}

	private void close(Connection connection) {
		try {
			if (connection != null)
				connection.close();
		} catch (SQLException ex) {
			ex.printStackTrace();
		}
	}

	private Boat extractBoat(ResultSet rs) throws SQLException {
		Boat item = new Boat();
		item.setId(rs.getLong(FIELD_ID));
		item.setName(rs.getString(FIELD_NAME));
		item.setType(rs.getString(FIELD_TYPE));
		item.setDate(new Date(rs.getLong(FIELD_DATE)));
		return item;
	}
}
